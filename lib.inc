%define SYSCALL_READ 0
%define SYSCALL_WRITE 1
%define SYSCALL_EXIT 60
%define STDIN 0
%define STDOUT 1

section .text
 
; Принимает код возврата и завершает текущий процесс
exit:
	mov rax, SYSCALL_EXIT
	syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
.counter:
	cmp byte[rdi + rax], 0
	je .end
	inc rax
	jmp .counter
.end:	
	ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	pop rsi
	mov rdi, STDOUT
	mov rdx, rax 
	mov rax, SYSCALL_WRITE
	syscall
	ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, `\n`
	
	
; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rax, SYSCALL_WRITE
	mov rsi, rsp
	mov rdi, STDOUT
	mov rdx, 1
	syscall
	pop rdi
	ret
	

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	cmp rdi, 0
	jge print_uint
	push rdi
	mov dil, '-'
	call print_char
	pop rdi
	neg rdi
	

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov r10, rsp
	push 0
	mov rax, rdi
	mov r9, 10
	xor rdx, rdx
.loop:
	div r9
	add dl, '0'
	dec rsp
	mov byte[rsp], dl
	xor rdx, rdx
	test rax, rax
	jne .loop
.end:
	mov rdi, rsp
	push r10
	call print_string
	pop rsp
	ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
.loop:
        mov dl, byte[rdi]
        cmp dl, byte[rsi]
        jne .neq
        inc rdi
        inc rsi
        test dl, dl
        je .eq
        jmp .loop
.neq:
        xor rax, rax
        ret
.eq:
        mov rax, 1
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
        push 0 
        mov rax, SYSCALL_READ 	;xor rax, rax
        mov rdi, STDIN 		;xor rdi, rdi
        mov rsi, rsp
        mov rdx, 1
        syscall  
        pop rax
        ret

	
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	push rbx
	push r12
	push r13
	xor rbx, rbx
	mov r12, rdi
	mov r13, rsi      
.loop:
	call read_char
	cmp rax, ' '
	je .skip
	cmp rax, `\n`
	je .skip
	cmp rax, `\t`
	je .skip
	test rax, rax
	je .end
	cmp rbx, r13
	jge .bufferof
	mov byte[r12 + rbx], al
	inc rbx
	jmp .loop
.skip:
    test rbx, rbx
    je .loop
.end:
    mov byte[r12 + rbx], 0
    mov rax, r12
	jmp .stop
.bufferof:
	xor rax, rax
.stop:
	mov rdx, rbx
	pop r13
	pop r12
	pop rbx
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rcx, rcx
	xor rax, rax	
	mov r8, 10; radix
.loop:	
	mov r9b, [rdi + rcx]
	cmp r9, '0'
	jl .not_a_num
	cmp r9, '9'
	jg .not_a_num
	sub r9, '0'
	mul r8
	add rax, r9
	inc rcx
	jmp .loop
.not_a_num:
	test rcx, rcx
	je .fail
	mov rdx, rcx
	ret
.fail:
	xor rax, rax
	xor rdx, rdx
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	mov r9b, byte[rdi]
	cmp r9b, '+'
	je .sign
	cmp r9b, '-'
	je .sign
	jmp parse_uint
.sign:
	inc rdi
	push r9
	call parse_uint
	pop r9
	test rdx, rdx
	je .end
	inc rdx
	cmp r9b, '+'
	je .end
	neg rax
.end:
	ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax; pointer to given string
.loop:
	cmp rax, rdx
	jge .fail
	mov cl, byte[rdi + rax]
	mov byte[rsi + rax], cl
	inc rax
	test cl, cl
	je .end
	jmp .loop
.fail:	
	xor rax, rax
.end:
	ret ; returns length with 0-terminator
